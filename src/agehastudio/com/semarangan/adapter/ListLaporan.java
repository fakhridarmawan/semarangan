package agehastudio.com.semarangan.adapter;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import agehastudio.com.drawernavigationtabs.R;
import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class ListLaporan extends BaseAdapter {
	private Activity activity;
	private ArrayList<HashMap<String, String>> LaporanList;
	private static LayoutInflater inflater=null;

  	public ListLaporan(Activity a, ArrayList<HashMap<String, String>> objects) {
	    activity = a;
	    LaporanList = objects;
	    inflater = (LayoutInflater)activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return LaporanList.size();
	}
	
	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return position;
	}
	
	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}
	
	public View getView(int position, View convertView, ViewGroup parent) {
        View vi=convertView;
        if(convertView==null)
            vi = inflater.inflate(R.layout.row_laporan, null);
 
        TextView nama = (TextView)vi.findViewById(R.id.nama);
        TextView txtlaporan = (TextView)vi.findViewById(R.id.laporan);
        ImageView profil=(ImageView)vi.findViewById(R.id.foto_profil); 
        ImageView laporan=(ImageView)vi.findViewById(R.id.foto_laporan); 
 
        HashMap<String, String> list_laporan = new HashMap<String, String>();
        list_laporan = LaporanList.get(position);
 
        // Setting all values in listview
        nama.setText(list_laporan.get("nama"));
        txtlaporan.setText(list_laporan.get("isi_laporan"));
        
        //Toast.makeText(activity, berita.get("laporan"), Toast.LENGTH_SHORT).show();
        
        new DownloadImageTask(profil).execute(list_laporan.get("profil"));
        new DownloadImageTask(laporan).execute(list_laporan.get("laporan"));
        
        return vi;
    }
	
	class DownloadImageTask extends AsyncTask<String, Void, Bitmap> {
	    ImageView bmImage;

	    public DownloadImageTask(ImageView bmImage) {
	        this.bmImage = bmImage;
	    }

	    protected Bitmap doInBackground(String... urls) {
	        String urldisplay = urls[0];
	        Bitmap mIcon11 = null;
	        try {
	            InputStream in = new java.net.URL(urldisplay).openStream();
	            mIcon11 = BitmapFactory.decodeStream(in);
	        } catch (Exception e) {
	            
	        }
	        return mIcon11;
	    }

	    protected void onPostExecute(Bitmap result) {
	        bmImage.setImageBitmap(result);
	    }
	}
} 