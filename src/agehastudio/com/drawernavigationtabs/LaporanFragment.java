package agehastudio.com.drawernavigationtabs;


import java.util.ArrayList;
import java.util.HashMap;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import agehastudio.com.drawernavigationtabs.R;
import agehastudio.com.semarangan.adapter.JsonParser;
import agehastudio.com.semarangan.adapter.ListLaporan;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class LaporanFragment extends Fragment{
	
	private static String url = "http://192.168.56.101/orange/semarangan/laporan.php?user=1&format=json";
	
	// contacts JSONArray
	static JSONArray laporan;
	static ArrayList<HashMap<String, String>> LaporanList;
	
	static View rootView;
	static ListView listView;
	
	ListLaporan adapter;
	
	@Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
  
        //View rootView = inflater.inflate(R.layout.fragment_photos, container, false);
		
		rootView = inflater.inflate(R.layout.list_laporan, container, false);
          
		new MyAsyncTask().execute();
        
        listView = (ListView) rootView.findViewById(R.id.list_laporan);
        
        // Click event for single list row
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				// TODO Auto-generated method stub
				
				//String tnama = ((TextView) view.findViewById(R.id.id_post)).getText().toString();
				
				Bundle bundle=new Bundle();
                //bundle.putString("nama", tnama);
                //set Fragmentclass Arguments
                Fragment fragobj=new DetailBerita();
                fragobj.setArguments(bundle);
                
                // consider using Java coding conventions (upper first char class names!!!)
                FragmentTransaction transaction = getFragmentManager().beginTransaction();

                // Replace whatever is in the fragment_container view with this fragment,
                // and add the transaction to the back stack
                transaction.replace(R.id.content_frame, fragobj);
                transaction.addToBackStack(null);

                // Commit the transaction
                transaction.commit(); 
			}
        	
        });
                
        return rootView;
    }
	
	class MyAsyncTask extends AsyncTask<Void, Void, Void> {

		@Override
        protected Void doInBackground(Void... params) {
        	//TODO: your background code
        	// Creating JSON Parser instance
            JsonParser jParser = new JsonParser();
            
            // getting JSON string from URL
	        JSONObject json = jParser.getJSONFromUrl(url);
	        
	        // Create an array
	        LaporanList = new ArrayList<HashMap<String, String>>();
	        
	        try {
	            // Getting Array of Contacts
	            laporan = json.getJSONArray("posts");
	             
	            // looping through All Contacts
	            for(int i = 0; i < laporan.length(); i++){
	                JSONObject c = laporan.getJSONObject(i);
	                 
	                // Storing each json item in variable
	                JSONObject post = c.getJSONObject("post");
	                //String id = c.getString(TAG_ID);
	                //String name = c.getString(TAG_NAME);
	                //String email = c.getString(TAG_EMAIL);
	                //String address = c.getString(TAG_ADDRESS);
	                //String gender = c.getString(TAG_GENDER);
	                 
	                // Phone number is agin JSON Object
	                String nama = post.getString("fullname");
	                String profil= post.getString("picture");
	                String laporan= post.getString("filename");
	                String isi_laporan= post.getString("content");
	                //String home = phone.getString(TAG_PHONE_HOME);
	                //String office = phone.getString(TAG_PHONE_OFFICE);
	                
	                // creating new HashMap
	                HashMap<String, String> map = new HashMap<String, String>();
	                 
	                // adding each child node to HashMap key => value
	                map.put("nama", nama);
	                map.put("profil", profil);
	                map.put("laporan", laporan);
	                map.put("isi_laporan", isi_laporan);
	                //map.put(TAG_NAME, name);
	                //map.put(TAG_EMAIL, email);
	                //map.put(TAG_PHONE_MOBILE, mobile);
	 
	                // adding HashList to ArrayList
	                LaporanList.add(map);
	            }
	        } catch (JSONException e) {
	            e.printStackTrace();
	        }
	        
	        return null;
        }

        @Override
        protected void onPostExecute(Void response) {
            adapter = new ListLaporan(getActivity(), LaporanList);
            
            //setListAdapter(adapter);
            ListView tampil = (ListView) rootView.findViewById(R.id.list_laporan);
            tampil.setAdapter(adapter);
            
            // Close the progressdialog
            //mProgressDialog.dismiss();
        }
    }
}
