package agehastudio.com.drawernavigationtabs;


import agehastudio.com.drawernavigationtabs.R;
import agehastudio.com.semarangan.adapter.AbstractNavDrawerActivity;
import agehastudio.com.semarangan.adapter.NavDrawerAdapter;
import agehastudio.com.semarangan.model.NavDrawerActivityConfiguration;
import agehastudio.com.semarangan.model.NavDrawerItem;
import agehastudio.com.semarangan.model.NavMenuItem;
import agehastudio.com.semarangan.model.NavMenuSection;
import android.os.Bundle;
import android.app.Activity;
import android.content.res.Configuration;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

public class MainActivityCustomNavDrawer extends AbstractNavDrawerActivity {

	private DrawerLayout mDrawerLayout;
	private ListView mDrawerList;
	private ActionBarDrawerToggle mDrawerToggle;
	
	private CharSequence mDrawerTitle;
	private CharSequence mTitle;
	private String[] mDrawerItmes;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		if ( savedInstanceState == null ) {
            getSupportFragmentManager().beginTransaction().replace(R.id.content_frame, new TabbedActivity()).commit();
        }
	
	}
	
	@Override
    protected NavDrawerActivityConfiguration getNavDrawerConfiguration() {
       
        NavDrawerItem[] menu = new NavDrawerItem[] {
                NavMenuSection.create( 100, "Beranda"),
                NavMenuItem.create(101,"Lapor", "ic_home", true, this),
                NavMenuItem.create(102, "Berita", "ic_drawer", true, this),
                NavMenuSection.create(200, "Hendrar Prihadi"),
                NavMenuItem.create(201, "Profil", "ic_home", false, this),
                NavMenuItem.create(202, "Karya Professional", "ic_home", false, this),
                NavMenuItem.create(203, "Aktivitas Sosial", "ic_home", false, this),
		        NavMenuSection.create(300, "Semarang"),
		        NavMenuItem.create(301, "Semarang Masa Lalu", "ic_home", false, this),
		        NavMenuItem.create(302, "Semarang Masa Kini", "ic_home", false, this),
		        NavMenuSection.create(400, "Relawan"),
		        NavMenuItem.create(401, "Surat Untuk Relawan", "ic_home", false, this),
		        NavMenuItem.create(402, "Daftar Relawan", "ic_home", false, this),
		        NavMenuSection.create(500, "Pengaturan"),
		        NavMenuItem.create(501, "Profil User", "ic_home", false, this),
		        NavMenuItem.create(502, "F.A.Q", "ic_home", false, this),
		        NavMenuItem.create(503, "Saran dan Masukkan", "ic_home", false, this),
		        NavMenuItem.create(504, "Logout", "ic_home", false, this),
		        NavMenuItem.create(505, "Aplikasi Versi 1.0", "ic_home", false, this)};
       
        NavDrawerActivityConfiguration navDrawerActivityConfiguration = new NavDrawerActivityConfiguration();
        navDrawerActivityConfiguration.setMainLayout(R.layout.activity_main);
        navDrawerActivityConfiguration.setDrawerLayoutId(R.id.drawer_layout);
        navDrawerActivityConfiguration.setLeftDrawerId(R.id.left_drawer);
        navDrawerActivityConfiguration.setNavItems(menu);
        navDrawerActivityConfiguration.setDrawerShadow(R.drawable.drawer_shadow);      
        navDrawerActivityConfiguration.setDrawerOpenDesc(R.string.drawer_open);
        navDrawerActivityConfiguration.setDrawerCloseDesc(R.string.drawer_close);
        navDrawerActivityConfiguration.setBaseAdapter(
            new NavDrawerAdapter(this, R.layout.navdrawer_item, menu ));
        return navDrawerActivityConfiguration;
    }
   
    @Override
    protected void onNavItemSelected(int id) {
        switch ((int)id) {
        case 101:
            getSupportFragmentManager().beginTransaction().replace(R.id.content_frame, new TabbedActivity()).commit();
            break;
        case 102:
            getSupportFragmentManager().beginTransaction().replace(R.id.content_frame, new Berita()).commit();
            break;
        case 201:
            //getSupportFragmentManager().beginTransaction().replace(R.id.content_frame, new WebViewFragment()).commit();
        	getSupportFragmentManager().beginTransaction().replace(R.id.content_frame, new DummyFragment()).commit();
            break;
        case 202:
            getSupportFragmentManager().beginTransaction().replace(R.id.content_frame, new DummyFragment()).commit();
            break;
        case 203:
            getSupportFragmentManager().beginTransaction().replace(R.id.content_frame, new DummyFragment()).commit();
            break;
        case 301:
            getSupportFragmentManager().beginTransaction().replace(R.id.content_frame, new DummyFragment()).commit();
            break;
        case 302:
            getSupportFragmentManager().beginTransaction().replace(R.id.content_frame, new DummyFragment()).commit();
            break;
        case 401:
            getSupportFragmentManager().beginTransaction().replace(R.id.content_frame, new DummyFragment()).commit();
            break;
        case 402:
            getSupportFragmentManager().beginTransaction().replace(R.id.content_frame, new DummyFragment()).commit();
            break;
        case 501:
            getSupportFragmentManager().beginTransaction().replace(R.id.content_frame, new DummyFragment()).commit();
            break;
        case 502:
            getSupportFragmentManager().beginTransaction().replace(R.id.content_frame, new DummyFragment()).commit();
            break;
        case 503:
            getSupportFragmentManager().beginTransaction().replace(R.id.content_frame, new DummyFragment()).commit();
            break;
        case 505:
            getSupportFragmentManager().beginTransaction().replace(R.id.content_frame, new DummyFragment()).commit();
            break;
        }
    }
    
    /**
	 * A dummy fragment representing a section of the app, but that simply
	 * displays dummy text.
	 */
	public static class DummyFragment extends Fragment {
		/**
		 * The fragment argument representing the section number for this
		 * fragment.
		 */
		public static final String ARG_SECTION_NUMBER = "section_number";

		public DummyFragment() {
		}

		@Override
		public View onCreateView(LayoutInflater inflater, ViewGroup container,
				Bundle savedInstanceState) {
			View rootView = inflater.inflate(R.layout.fragment_nav_dummy,
					container, false);
			return rootView;
		}
	}
	
}
