package agehastudio.com.drawernavigationtabs;

import java.util.ArrayList;
import java.util.HashMap;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import agehastudio.com.drawernavigationtabs.LaporanFragment.MyAsyncTask;
import agehastudio.com.semarangan.adapter.JsonParser;
import agehastudio.com.semarangan.adapter.ListLaporan;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SimpleAdapter;

public class Statistika extends Fragment {
	
	private static String url = "http://192.168.56.101/orange/semarangan/statistik.php?user=1&format=json";
	private static String url2 = "http://192.168.56.101/orange/semarangan/pengguna.php?user=1&format=json";
	
	// contacts JSONArray
	static JSONArray statistik;
	static JSONArray pengguna;
	
	static ArrayList<HashMap<String, String>> StatistikList;
	static ArrayList<HashMap<String, String>> PenggunaList;
	
	static View rootView;
	static ListView listView;
	static ListView listView2;
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		rootView = inflater.inflate(R.layout.statistika,
				container, false);
		
		new MyAsyncTask().execute();
		new MyAsyncTask2().execute();
        
		return rootView;
	}
	
	class MyAsyncTask extends AsyncTask<Void, Void, Void> {

		@Override
        protected Void doInBackground(Void... params) {
        	//TODO: your background code
        	// Creating JSON Parser instance
            JsonParser jParser = new JsonParser();
            
            // getting JSON string from URL
	        JSONObject json = jParser.getJSONFromUrl(url);
	        
	        // Create an array
	        StatistikList = new ArrayList<HashMap<String, String>>();
	        
	        try {
	            // Getting Array of Contacts
	            statistik = json.getJSONArray("posts");
	             
	            // looping through All Contacts
	            for(int i = 0; i < statistik.length(); i++){
	                JSONObject c = statistik.getJSONObject(i);
	                 
	                // Storing each json item in variable
	                JSONObject post = c.getJSONObject("post");
	                //String id = c.getString(TAG_ID);
	                //String name = c.getString(TAG_NAME);
	                //String email = c.getString(TAG_EMAIL);
	                //String address = c.getString(TAG_ADDRESS);
	                //String gender = c.getString(TAG_GENDER);
	                 
	                // Phone number is agin JSON Object
	                String kategori = post.getString("name");
	                String jumlah= post.getString("jumlah");
	                //String home = phone.getString(TAG_PHONE_HOME);
	                //String office = phone.getString(TAG_PHONE_OFFICE);
	                
	                // creating new HashMap
	                HashMap<String, String> map = new HashMap<String, String>();
	                 
	                // adding each child node to HashMap key => value
	                map.put("kategori", kategori);
	                map.put("jumlah", jumlah);
	                //map.put(TAG_NAME, name);
	                //map.put(TAG_EMAIL, email);
	                //map.put(TAG_PHONE_MOBILE, mobile);
	 
	                // adding HashList to ArrayList
	                StatistikList.add(map);
	            }
	        } catch (JSONException e) {
	            e.printStackTrace();
	        }
	        
	        return null;
        }

        @Override
        protected void onPostExecute(Void response) {
        	ListAdapter adapter = new SimpleAdapter(getActivity(), StatistikList,
	                R.layout.statistika_item,
	                new String[] {"kategori","jumlah"}, new int[] {
	        		        R.id.kategori,R.id.jumlah});
            
            //setListAdapter(adapter);
            ListView tampil = (ListView) rootView.findViewById(R.id.list_statistik);
            tampil.setAdapter(adapter);
            
            // Close the progressdialog
            //mProgressDialog.dismiss();
        }
    }
	
	class MyAsyncTask2 extends AsyncTask<Void, Void, Void> {

		@Override
        protected Void doInBackground(Void... params) {
        	//TODO: your background code
        	// Creating JSON Parser instance
            JsonParser jParser = new JsonParser();
            
            // getting JSON string from URL
	        JSONObject json = jParser.getJSONFromUrl(url2);
	        
	        // Create an array
	        PenggunaList = new ArrayList<HashMap<String, String>>();
	        
	        try {
	            // Getting Array of Contacts
	            pengguna = json.getJSONArray("posts");
	             
	            // looping through All Contacts
	            for(int i = 0; i < pengguna.length(); i++){
	                JSONObject c = pengguna.getJSONObject(i);
	                 
	                // Storing each json item in variable
	                JSONObject post = c.getJSONObject("post");
	                //String id = c.getString(TAG_ID);
	                //String name = c.getString(TAG_NAME);
	                //String email = c.getString(TAG_EMAIL);
	                //String address = c.getString(TAG_ADDRESS);
	                //String gender = c.getString(TAG_GENDER);
	                 
	                // Phone number is agin JSON Object
	                String nama = post.getString("fullname");
	                String jumlah= post.getString("jumlah");
	                //String home = phone.getString(TAG_PHONE_HOME);
	                //String office = phone.getString(TAG_PHONE_OFFICE);
	                
	                // creating new HashMap
	                HashMap<String, String> map = new HashMap<String, String>();
	                 
	                // adding each child node to HashMap key => value
	                map.put("no", Integer.toString(i+1));
	                map.put("nama", nama);
	                map.put("jumlah", jumlah);
	                //map.put(TAG_NAME, name);
	                //map.put(TAG_EMAIL, email);
	                //map.put(TAG_PHONE_MOBILE, mobile);
	 
	                // adding HashList to ArrayList
	                PenggunaList.add(map);
	            }
	        } catch (JSONException e) {
	            e.printStackTrace();
	        }
	        
	        return null;
        }

        @Override
        protected void onPostExecute(Void response) {
        	ListAdapter adapter = new SimpleAdapter(getActivity(), PenggunaList,
	                R.layout.pengguna_item,
	                new String[] {"no","nama","jumlah"}, new int[] {
	        		        R.id.no,R.id.nama,R.id.jumlah});
            
            //setListAdapter(adapter);
            ListView tampil = (ListView) rootView.findViewById(R.id.list_pengguna);
            tampil.setAdapter(adapter);
            
            // Close the progressdialog
            //mProgressDialog.dismiss();
        }
    }
}
