package agehastudio.com.drawernavigationtabs;


import agehastudio.com.drawernavigationtabs.R;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

public class DetailBerita extends Fragment{
	
	@Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
  
        View rootView = inflater.inflate(R.layout.detail_berita, container, false);
        
        TextView vtitle = (TextView) rootView.findViewById(R.id.title);
        TextView visi= (TextView) rootView.findViewById(R.id.isi);
        
        String title = getArguments().getString("title");
        String isi   = getArguments().getString("isi");
        
        vtitle.setText(title);
        visi.setText(isi);
        
        return rootView;
    }
	
}
