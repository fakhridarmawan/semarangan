package agehastudio.com.drawernavigationtabs;

import agehastudio.com.semarangan.adapter.JsonParser;

import java.util.ArrayList;
import java.util.HashMap;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import android.widget.Toast;

public class Berita extends Fragment {
	private static String url = "http://192.168.56.101/orange/semarangan/service.php?user=1&format=json";
	
	// contacts JSONArray
	static JSONArray berita;
	static ArrayList<HashMap<String, String>> BeritaList;
	
	static View rootView;
	static ListView listView;
	
	@Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
        Bundle savedInstanceState) {

        rootView = inflater.inflate(R.layout.berita, container, false);
        
        new MyAsyncTask().execute();
        
        listView = (ListView) rootView.findViewById(R.id.list);
        
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
                //Toast.makeText(getActivity(), "You Clicked at "+listView.getTag(), Toast.LENGTH_SHORT).show();
            	
            	// getting values from selected ListItem
                String title = ((TextView) view.findViewById(R.id.title)).getText().toString();
                String resume = ((TextView) view.findViewById(R.id.resume)).getText().toString();
                //String description = ((TextView) view.findViewById(R.id.product_mobile)).getText().toString();
                
                Context context = getActivity();
                CharSequence text = title;
                int duration = Toast.LENGTH_SHORT;

                Toast toast = Toast.makeText(context, text, duration);
                toast.show();
                
                // Starting new intent
                //Intent in = new Intent(getActivity(), SingleList.this);
                //in.putExtra("nama", name);
                //in.putExtra("picture", pic);
                //in.putExtra(TAG_EMAIL, cost);
                //in.putExtra(TAG_PHONE_MOBILE, description);
                //startActivity(in);
                
                // Create new fragment and transaction
                //Fragment newFragment = new DetailBerita();
                // consider using Java coding conventions (upper first char class names!!!)
                //FragmentTransaction transaction = getFragmentManager().beginTransaction();

                // Replace whatever is in the fragment_container view with this fragment,
                // and add the transaction to the back stack
                //transaction.replace(R.id.content_frame, newFragment);
                //transaction.addToBackStack(null);

                // Commit the transaction
                //transaction.commit(); 
                
                Bundle bundle=new Bundle();
                bundle.putString("title", title);
                bundle.putString("isi", resume);
                //set Fragmentclass Arguments
                Fragment fragobj=new DetailBerita();
                fragobj.setArguments(bundle);
                
                // consider using Java coding conventions (upper first char class names!!!)
                FragmentTransaction transaction = getFragmentManager().beginTransaction();

                // Replace whatever is in the fragment_container view with this fragment,
                // and add the transaction to the back stack
                transaction.replace(R.id.content_frame, fragobj);
                transaction.addToBackStack(null);

                // Commit the transaction
                transaction.commit(); 

            }
        });
        
        return rootView;
    }
	
	class MyAsyncTask extends AsyncTask<Void, Void, Void> {

		@Override
        protected Void doInBackground(Void... params) {
        	//TODO: your background code
        	// Creating JSON Parser instance
            JsonParser jParser = new JsonParser();
            
            // getting JSON string from URL
	        JSONObject json = jParser.getJSONFromUrl(url);
	        
	        // Create an array
	        BeritaList = new ArrayList<HashMap<String, String>>();
	        
	        try {
	            // Getting Array of Contacts
	            berita = json.getJSONArray("posts");
	             
	            // looping through All Contacts
	            for(int i = 0; i < berita.length(); i++){
	                JSONObject c = berita.getJSONObject(i);
	                 
	                // Storing each json item in variable
	                JSONObject post = c.getJSONObject("post");
	                //String id = c.getString(TAG_ID);
	                //String name = c.getString(TAG_NAME);
	                //String email = c.getString(TAG_EMAIL);
	                //String address = c.getString(TAG_ADDRESS);
	                //String gender = c.getString(TAG_GENDER);
	                 
	                // Phone number is agin JSON Object
	                String nama = post.getString("title");
	                //JSONObject phone = c.getJSONObject(TAG_PHONE);
	                String resume= post.getString("resume");
	                //String home = phone.getString(TAG_PHONE_HOME);
	                //String office = phone.getString(TAG_PHONE_OFFICE);
	                
	                // creating new HashMap
	                HashMap<String, String> map = new HashMap<String, String>();
	                 
	                // adding each child node to HashMap key => value
	                map.put("title", nama);
	                map.put("resume", resume);
	                //map.put(TAG_NAME, name);
	                //map.put(TAG_EMAIL, email);
	                //map.put(TAG_PHONE_MOBILE, mobile);
	 
	                // adding HashList to ArrayList
	                BeritaList.add(map);
	            }
	        } catch (JSONException e) {
	            e.printStackTrace();
	        }
	        
	        return null;
        }

        @Override
        protected void onPostExecute(Void response) {
            ListAdapter adapter = new SimpleAdapter(getActivity(), BeritaList,
	                R.layout.row_berita,
	                new String[] {"title","resume"}, new int[] {
	        		        R.id.title,R.id.resume});
            
            //setListAdapter(adapter);
            ListView tampil = (ListView) rootView.findViewById(R.id.list);
            tampil.setAdapter(adapter);
            
            // Close the progressdialog
            //mProgressDialog.dismiss();
        }
    }
}
